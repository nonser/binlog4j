package com.gitee.Jmysy.binlog4j.springboot.starter;

import com.gitee.Jmysy.binlog4j.core.config.RedisConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@ConditionalOnProperty(prefix = "spring.binlog4j", name = "enable", havingValue = "true")
@EnableConfigurationProperties(Binlog4jAutoProperties.class)
public class Binlog4jAutoConfiguration {

    public Binlog4jAutoConfiguration(Binlog4jAutoProperties properties) {
        RedisConfig redisConfig = properties.getRedisConfig();
        properties.getClientConfigs().forEach((clientName, clientConfig) -> {
            if (redisConfig != null && clientConfig.getRedisConfig() == null) {
                clientConfig.setRedisConfig(redisConfig);
            }
        });
    }

    @Bean
    public Binlog4jInitializationBeanProcessor binlog4jAutoInitializing(Binlog4jAutoProperties properties) {
        return new Binlog4jInitializationBeanProcessor(properties.getClientConfigs());
    }
}
