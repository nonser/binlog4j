package com.gitee.Jmysy.binlog4j.springboot.starter.example.domain;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BinlogPosition {

    private LocalDate createTime;

    private String text;
}
